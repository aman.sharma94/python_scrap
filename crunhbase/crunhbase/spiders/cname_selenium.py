# -*- coding: utf-8 -*-
import scrapy
from scrapy.selector import Selector
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from shutil import which


class CompanynameSelenium(scrapy.Spider):
    name = 'cname_selenium'
    allowed_domains = ['www.crunchbase.com']
    start_urls = [
        'https://www.crunchbase.com'
    ]
    user_agent = "user-agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
    
    def __init__(self):
        chrome_option = Options()
        chrome_path = which('chromedriver')
        chrome_option.add_argument("--headless")
        chrome_option.add_argument("start-maximized")
        chrome_option.add_experimental_option("excludeSwitches", ["enable-automation"])
        chrome_option.add_experimental_option('useAutomationExtension', False)
        driver = webdriver.Chrome(options=chrome_option,executable_path=chrome_path)
        driver.get('https://www.crunchbase.com')
        self.html = driver.page_source
        print(driver.page_source)
        # driver.close()

        
    def parse(self, response):
        resp = Selector(text=self.html)
        print(resp)
        