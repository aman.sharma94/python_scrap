# -*- coding: utf-8 -*-
import scrapy

class CompanyNameSpider(scrapy.Spider):
    name = 'company_name'
    allowed_domains = ['www.crunchbase.com']
    user_agent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1"

    def start_requests(self):
        yield scrapy.Request(url='https://www.crunchbase.com/search/organization.companies/a4df61cb5e9dbb4ba533879a674eef8c', headers={
            'User-Agent': self.user_agent
        })
        
    def parse(self, response):
        company_name =  response.xpath("//div[@class='body-wrapper']/grid-row[@class='ng-star-inserted']")
        for row in company_name:

            c_name = row.xpath('.//grid-cell[@class="sticky-column-2 column-id-identifier ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/identifier-formatter[@class="ng-star-inserted"]/a/div/div[@class="flex-no-grow cb-overflow-ellipsis identifier-label"]/text()').get()
            url = row.xpath('.//grid-cell[@class="sticky-column-2 column-id-identifier ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/identifier-formatter[@class="ng-star-inserted"]/a/@href').get()
            
            yield response.follow(url=url,callback=self.parseCompany,meta={"company_name":c_name})

    
    def parseCompany(self,response):
                company_name = response.request.meta['company_name']
                section = response.xpath('//div[@class="section-layout-content"][1]')
                legal_name = section.xpath('.//fields-card[@class="ng-star-inserted"][1]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"]/field-formatter/blob-formatter/span[@class="ng-star-inserted"]/text()').get()
                if(legal_name is None):
                    legal_name = section.xpath('.//fields-card[@class="ng-star-inserted"][1]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"]/field-formatter/blob-formatter/span[@class="field-value flex-100 flex-gt-sm-75 last-field ng-star-inserted"]/text()').get()
                contact_email = section.xpath('.//fields-card[@class="ng-star-inserted"][4]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"]/field-formatter/blob-formatter/span[@class="ng-star-inserted"]/text()').get()
                contact_phone = section.xpath('.//fields-card[@class="ng-star-inserted"][4]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 last-field ng-star-inserted"]/field-formatter/blob-formatter/span[@class="ng-star-inserted"]/text()').get()
                website = section.xpath('.//fields-card[@class="ng-star-inserted"][4]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][1]/field-formatter/link-formatter/a/@href').get()
                facebook = section.xpath('.//fields-card[@class="ng-star-inserted"][4]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][2]/field-formatter/link-formatter/a/@href').get() 
                linkedin = section.xpath('.//fields-card[@class="ng-star-inserted"][4]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][3]/field-formatter/link-formatter/a/@href').get()
                twitter = section.xpath('.//fields-card[@class="ng-star-inserted"][4]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][4]/field-formatter/link-formatter/a/@href').get()
                
                if(contact_email is None):
                    contact_email = section.xpath('.//fields-card[@class="ng-star-inserted"][3]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"]/field-formatter/blob-formatter/span[@class="ng-star-inserted"]/text()').get()
                    contact_phone = section.xpath('.//fields-card[@class="ng-star-inserted"][3]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 last-field ng-star-inserted"]/field-formatter/blob-formatter/span[@class="ng-star-inserted"]/text()').get()
                    website = section.xpath('.//fields-card[@class="ng-star-inserted"][3]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][1]/field-formatter/link-formatter/a/@href').get()
                    facebook = section.xpath('.//fields-card[@class="ng-star-inserted"][3]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][2]/field-formatter/link-formatter/a/@href').get() 
                    linkedin = section.xpath('.//fields-card[@class="ng-star-inserted"][3]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][3]/field-formatter/link-formatter/a/@href').get()
                    twitter = section.xpath('.//fields-card[@class="ng-star-inserted"][3]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][4]/field-formatter/link-formatter/a/@href').get()
                

                yield {
                       'company_name':company_name,
                       'legal_name':legal_name,
                       'contact_email':contact_email,
                       'contact_phone':contact_phone,
                       'website':website,
                       'facebook':facebook,
                       'linkedin':linkedin,
                       'twitter':twitter


                }