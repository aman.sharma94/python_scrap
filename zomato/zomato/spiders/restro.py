# -*- coding: utf-8 -*-
import scrapy


class RestroSpider(scrapy.Spider):
    name = 'restro'
    allowed_domains = ['www.zomato.com']
    user_agent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1"

    def start_requests(self):
        yield scrapy.Request(url='https://www.zomato.com/ncr', headers={
            'User-Agent': self.user_agent
        })

    def parse(self, response):
        print(response)
