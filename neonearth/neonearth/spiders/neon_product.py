# -*- coding: utf-8 -*-
import scrapy


class NeonProductSpider(scrapy.Spider):
    name = 'neon_product'
    allowed_domains = ['www.neonearth.com']
    start_urls = ['https://www.neonearth.com/shop/wall-surfaces/wallmural.html']

    def parse(self, response):
        category = response.xpath('//li[@class="item product product-item product-item-dynamic"]')
        link = response.xpath('//div[@class="toolbar toolbar-products"][2]/div[@class="pages"]/ul[@class="items pages-items"]/li[@class="item pages-item-next"]/a[@class="action  next"]/@href').get()
        for categorylist in category:
            name = categorylist.xpath('.//div[@class="product-item-info"]/div[@class="product details product-item-details"]/strong[@class="product name product-item-name"]/a[@class="product-item-link"]/text()').get()
            image_link = categorylist.xpath('.//div[@class="product-item-info"]/a[@class="product photo product-item-photo hover-imaeg-dc"]/span[@class="product-image-container"]/span[@class="product-image-wrapper"]/img[@class="product-image-photo"]/@src').get()
            product_price = categorylist.xpath('.//div[@class="product-item-info"]/div[@class="product details product-item-details"]/div[@class="price-box price-final_price"]/span[@class="normal-price"]/span[@class="price-container price-final_price tax weee"]/span[@class="price-wrapper "]/span[@class="price"]/text()').get()


            yield {
                'productname':name.strip(),
                'image_link':image_link,
                'product_price':product_price
            }

        yield response.follow(url=link,callback=self.parse)        
