# -*- coding: utf-8 -*-
import scrapy


class NeonSpider(scrapy.Spider):
    name = 'neon'
    allowed_domains = ['www.neonearth.com']
    start_urls = ['https://www.neonearth.com/shop/wall-surfaces/wallpaper.html']

    def parse(self, response):
        category = response.xpath('//span[@class="commercepundit_megamenu_title_lv-2"]')
        for categorylist in category:
            name = categorylist.xpath('.//text()').get()

            yield {
                'category_name':name
            }

