# -*- coding: utf-8 -*-
import scrapy
import selenium
from scrapy.selector import Selector
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from shutil import which
from time  import sleep

class LoginSpider(scrapy.Spider):
    name = 'login'
    allowed_domains = ['www.crunhbase.com']
    start_urls = [
        'http://www.crunhbase.com'
    ]
    def __init__(self):
        self.header = {'User-Agent':'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'}
        chrome_option = Options()
        chrome_path = which('chromedriver')
        self.driver = webdriver.Chrome(options=chrome_option,executable_path=chrome_path)
        self.driver.get('https://www.crunchbase.com')
        sleep(2)
        self.html = self.driver.page_source
        
        

    def parse(self, response):
        scrapy_selector = Selector(text= self.html)
        login = scrapy_selector.xpath('//nav-action-item[@class="ng-star-inserted"]/nav-header-action-item/a/@href').get()
        print(login)
        sleep(2)
        self.driver.get('https://www.crunchbase.com'+login)
        sleep(2)
        Email = self.driver.find_element_by_id('mat-input-1')
        Password = self.driver.find_element_by_id('mat-input-2')
        sleep(2)
        Email.send_keys('vikasdahiya10@gmail.com')
        sleep(2)
        Password.send_keys('control1028')
        sleep(2)
        Login_btn = self.driver.find_element_by_xpath('//button[@class="cb-text-transform-upper mat-raised-button mat-button-base mat-primary"]')
        Login_btn.click()

        sleep(5)
        self.driver.get('https://www.crunchbase.com/discover/organization.companies/ad0c2db94c3e95263e0fc4de4295528e')

        self.cView = self.driver.page_source

        CnameSelector = Selector(text = self.cView)
        company_name =  CnameSelector.xpath("//div[@class='body-wrapper']/grid-row[@class='ng-star-inserted']")
        # for row in company_name:
        row = company_name[0]
        c_name = row.xpath('.//grid-cell[@class="sticky-column-2 column-id-identifier ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/identifier-formatter[@class="ng-star-inserted"]/a/div/div[@class="flex-no-grow cb-overflow-ellipsis identifier-label"]/text()').get()
        url = row.xpath('.//grid-cell[@class="sticky-column-2 column-id-identifier ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/identifier-formatter[@class="ng-star-inserted"]/a/@href').get()
        sleep(10)
        yield response.follow(url=url,callback=self.parseCompany,meta={"company_name":c_name})


    def parseCompany(self,response):

                CompnayDetials = self.driver.page_source
                CompnayDetialsSelector = Selector(text=CompnayDetials)
                company_name = response.request.meta['company_name']
                section = CompnayDetialsSelector.xpath('//div[@class="section-layout-content"][1]')
                legal_name = section.xpath('.//fields-card[@class="ng-star-inserted"][1]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"]/field-formatter/blob-formatter/span[@class="ng-star-inserted"]/text()').get()
                if(legal_name is None):
                    legal_name = section.xpath('.//fields-card[@class="ng-star-inserted"][1]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"]/field-formatter/blob-formatter/span[@class="field-value flex-100 flex-gt-sm-75 last-field ng-star-inserted"]/text()').get()
                contact_email = section.xpath('.//fields-card[@class="ng-star-inserted"][4]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"]/field-formatter/blob-formatter/span[@class="ng-star-inserted"]/text()').get()
                contact_phone = section.xpath('.//fields-card[@class="ng-star-inserted"][4]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 last-field ng-star-inserted"]/field-formatter/blob-formatter/span[@class="ng-star-inserted"]/text()').get()
                website = section.xpath('.//fields-card[@class="ng-star-inserted"][4]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][1]/field-formatter/link-formatter/a/@href').get()
                facebook = section.xpath('.//fields-card[@class="ng-star-inserted"][4]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][2]/field-formatter/link-formatter/a/@href').get() 
                linkedin = section.xpath('.//fields-card[@class="ng-star-inserted"][4]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][3]/field-formatter/link-formatter/a/@href').get()
                twitter = section.xpath('.//fields-card[@class="ng-star-inserted"][4]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][4]/field-formatter/link-formatter/a/@href').get()
                
                if(contact_email is None):
                    contact_email = section.xpath('.//fields-card[@class="ng-star-inserted"][3]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"]/field-formatter/blob-formatter/span[@class="ng-star-inserted"]/text()').get()
                    contact_phone = section.xpath('.//fields-card[@class="ng-star-inserted"][3]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 last-field ng-star-inserted"]/field-formatter/blob-formatter/span[@class="ng-star-inserted"]/text()').get()
                    website = section.xpath('.//fields-card[@class="ng-star-inserted"][3]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][1]/field-formatter/link-formatter/a/@href').get()
                    facebook = section.xpath('.//fields-card[@class="ng-star-inserted"][3]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][2]/field-formatter/link-formatter/a/@href').get() 
                    linkedin = section.xpath('.//fields-card[@class="ng-star-inserted"][3]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][3]/field-formatter/link-formatter/a/@href').get()
                    twitter = section.xpath('.//fields-card[@class="ng-star-inserted"][3]/div[@class="layout-wrap layout-row"]/span[@class="field-value flex-100 flex-gt-sm-75 ng-star-inserted"][4]/field-formatter/link-formatter/a/@href').get()
                

                yield {
                       'company_name':company_name,
                       'legal_name':legal_name,
                       'contact_email':contact_email,
                       'contact_phone':contact_phone,
                       'website':website,
                       'facebook':facebook,
                       'linkedin':linkedin,
                       'twitter':twitter


                }        



        
