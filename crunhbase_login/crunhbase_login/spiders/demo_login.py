# -*- coding: utf-8 -*-
import scrapy
import selenium
from scrapy.selector import Selector
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from shutil import which
from time  import sleep
from scrapy.utils.response import open_in_browser
from ..items import CrunhbaseLoginItem
from scrapy.loader import ItemLoader

class DemoLoginSpider(scrapy.Spider):
    name = 'demo_login'
    allowed_domains = ['www.crunchbase.com']
    start_urls = ['http://www.crunchbase.com']
    user_agent = "user-agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"
    def start_requests(self):
        yield scrapy.Request(url='https://www.crunchbase.com',callback=self.parse,headers={
            'User-Agent':'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'
        })
    def __init__(self):
        self.header = {'User-Agent':'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'}
        chrome_option = Options()
        chrome_path = which('chromedriver')
        self.driver = webdriver.Chrome(executable_path=chrome_path,options=chrome_option)
        self.driver.get('https://www.crunchbase.com')
        sleep(5)
        self.html = self.driver.page_source
        scrapy_selector = Selector(text= self.html)
        login = scrapy_selector.xpath('//nav-action-item[@class="ng-star-inserted"]/nav-header-action-item/a/@href').get()
        sleep(2)
        if login is not None:
            self.driver.get('https://www.crunchbase.com'+login)
            sleep(3)
            Email = self.driver.find_element_by_id('mat-input-1')
            Password = self.driver.find_element_by_id('mat-input-2')
            sleep(2)
            Email.send_keys('vikasdahiya10@gmail.com')
            sleep(2)
            Password.send_keys('control1028')
            sleep(2)
            Login_btn = self.driver.find_element_by_xpath('//button[@class="cb-text-transform-upper mat-raised-button mat-button-base mat-primary"]')
            Login_btn.click()
        else:
            self.driver.close()
        sleep(5)
        self.driver.get('https://www.crunchbase.com/discover/organization.companies/ad0c2db94c3e95263e0fc4de4295528e')

        self.cView = self.driver.page_source
        
        

    def parse(self, response):
        if(self.cView !=''):
            CnameSelector = Selector(text = self.cView)
            company_name =  CnameSelector.xpath("//div[@class='body-wrapper']/grid-row[@class='ng-star-inserted']")
        else:
            sleep(15)
            self.driver.get(response.url)
            self.cView = self.driver.page_source
            sleep(15)
            CnameSelector = Selector(text = self.cView)
            company_name =  CnameSelector.xpath("//div[@class='body-wrapper']/grid-row[@class='ng-star-inserted']")
        
        for row in company_name:
            loader = ItemLoader(item=CrunhbaseLoginItem(),selector=row,response=response)
            loader.add_xpath('company_name','.//grid-cell[@class="sticky-column-2 column-id-identifier ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/identifier-formatter[@class="ng-star-inserted"]/a/div/div[@class="flex-no-grow cb-overflow-ellipsis identifier-label"]/text()')
            loader.add_xpath('contact_email','.//grid-cell[@class="column-id-contact_email ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/blob-formatter[@class="ng-star-inserted"]/span[@class="ng-star-inserted"]/text()')
            loader.add_xpath('url','.//grid-cell[@class="sticky-column-2 column-id-identifier ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/identifier-formatter[@class="ng-star-inserted"]/a/@href')
            loader.add_xpath('phone_number','.//grid-cell[@class="column-id-phone_number ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/blob-formatter[@class="ng-star-inserted"]/span[@class="ng-star-inserted"]/text()')
            loader.add_xpath('facebook','.//grid-cell[@class="column-id-facebook ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/link-formatter[@class="ng-star-inserted"]/a/@href')
            loader.add_xpath('linkedn','.//grid-cell[@class="column-id-linkedin ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/link-formatter[@class="ng-star-inserted"]/a/@href')
            
            # c_name = row.xpath('.//grid-cell[@class="sticky-column-2 column-id-identifier ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/identifier-formatter[@class="ng-star-inserted"]/a/div/div[@class="flex-no-grow cb-overflow-ellipsis identifier-label"]/text()').get()
            # contact_email = row.xpath('.//grid-cell[@class="column-id-contact_email ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/blob-formatter[@class="ng-star-inserted"]/span[@class="ng-star-inserted"]/text()').get()
            # url = row.xpath('.//grid-cell[@class="sticky-column-2 column-id-identifier ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/identifier-formatter[@class="ng-star-inserted"]/a/@href').get()
            # phone_number=row.xpath('.//grid-cell[@class="column-id-phone_number ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/blob-formatter[@class="ng-star-inserted"]/span[@class="ng-star-inserted"]/text()').get()
            # facebook =row.xpath('.//grid-cell[@class="column-id-facebook ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/link-formatter[@class="ng-star-inserted"]/a/@href').get()
            # linkedn = facebook =row.xpath('.//grid-cell[@class="column-id-linkedin ng-star-inserted"][1]/div[@class="cb-absolute-to-cover layout-row layout-align-start-center"]/field-formatter[@class="cb-margin-medium-horizontal"]/link-formatter[@class="ng-star-inserted"]/a/@href').get()
            
            # Sitem['company_name'] = c_name
            # Sitem['contact_email'] = contact_email
            # Sitem['url'] = url
            # Sitem['phone_number'] = phone_number
            # Sitem['facebook'] = facebook
            # Sitem['linkedn'] = linkedn
            
            # yield Sitem
            yield loader.load_item()
           
        #     yield {
        #         'company_name':c_name,
        #         'url':response.urljoin(url),
        #         'contact_email':contact_email,
        #         'phone_number':phone_number,
        #         'facebook':facebook,
        #         'linkedn':linkedn
        #     }
        # nextPage = CnameSelector.xpath('//div[@class="flex layout-row layout-wrap layout-align-start-center"]/results-info[@class="flex-none hide show-gt-xs"]/h3[@class="component--results-info"]/a[@class="page-button-next mat-button mat-button-base mat-primary ng-star-inserted"]/@href').get()
        # if(nextPage != '/discover/organization.companies/035e8a4bec444d1c6db624f10d7460b9'):
        #     newUrl = response.urljoin(nextPage)
        #     sleep(15)
        #     self.cView = ''
        #     yield scrapy.Request(url=newUrl,callback=self.parse,headers={'User-Agent':'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'},dont_filter=True)