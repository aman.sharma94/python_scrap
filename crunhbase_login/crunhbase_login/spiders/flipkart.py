# -*- coding: utf-8 -*-
import scrapy


class FlipkartSpider(scrapy.Spider):
    name = 'flipkart'
    allowed_domains = ['www.flipkart.com']
    start_urls = ['https://www.flipkart.com/home-kitchen/home-appliances/washing-machines/fully-automatic-front-load~function/pr?sid=j9e%2Cabm%2C8qx&otracker=nmenu_sub_TVs%20%26%20Appliances_0_Fully%20Automatic%20Front%20Load']

    def parse(self, response):
        ProductDiv = response.xpath('//div[@class="_1HmYoV _35HD7C"][2]')
        productColumn = ProductDiv.xpath('.//div[@class="bhgxx2 col-12-12"]')
        for row in productColumn:
            
            p_name = row.xpath('.//div[@class="_3O0U0u"]/div/div[@class="_1UoZlX"]/a/div[@class="_1-2Iqu row"]/div[@class="col col-7-12"]/div[@class="_3wU53n"]/text()').get()
            rating = row.xpath('.//div[@class="_3O0U0u"]/div/div[@class="_1UoZlX"]/a/div[@class="_1-2Iqu row"]/div[@class="col col-7-12"]/div[@class="niH0FQ"]/span[@class="_2_KrJI"]/div[@class="hGSR34"]/text()').get()
            yield {
                'pname':p_name,
                'rating':rating
            }
