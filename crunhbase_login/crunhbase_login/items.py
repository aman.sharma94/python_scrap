# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import TakeFirst 


class CrunhbaseLoginItem(scrapy.Item):
    company_name = scrapy.Field(
        output_processors = TakeFirst()
    )
    url = scrapy.Field(
        output_processors = TakeFirst()
    )
    contact_email = scrapy.Field(
        output_processors = TakeFirst()
    )
    phone_number = scrapy.Field(
        output_processors = TakeFirst()
    )
    facebook = scrapy.Field(
        output_processors = TakeFirst()
    )
    linkedn = scrapy.Field(
        output_processors = TakeFirst()
    )
